# Feature Row Component

This component is used to display featured items for the dgx-homepage app.

## Version
> v0.2.0

## Usage

> Require `dgx-feature-row-component` as a dependency in your `package.json` file.

```sh
"dgx-feature-row-component": "git+ssh://git@bitbucket.org/NYPL/dgx-feature-row-component.git#master"
```

> Once installed, import the component in your React Application.

```sh
import FeatureRow from 'dgx-feature-row-component';
```

## Props

> You may initialize the component with the following properties:

```sh
<FeatureRow
  id="featureRow" ## String
  className="featureRow" ## String
  items={[]} ## Array of feature items (default: [])
  itemsToDisplay={4} ## Number of feature items to display (default: 2)
  error={true/false} ## Boolean flag to determine if an error occurred
  errorMessage={"We are sorry. Information is not available for this feature."} ## Error message to display when no items are available
  gaClickEvent={gaClickFunc()} ## Function for Google Analytics click events. The Action is determined by the gaActionText property and concatenated with the item number. The Label is the URL for each feature element
  gaActionText="Of Note" ## String representing the base of what the Action field will be on each feature item
/>
```

> An example of an individual item object:

```sh
  {
    title: {
      en: {
        text: 'title01',
      },
      es: {},
    },
    description: {
      en: {
        text: 'description01',
      },
    },
    image: {
      rectangularImage: {
        'full-uri': 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
        description: 'This is the image description',
        alt: 'This is the image description',
      },
    },
    link: 'http://www.nypl.org',
  }
```
