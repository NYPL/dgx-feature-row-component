import React from 'react';
import PropTypes from 'prop-types';

const FeatureRow = ({
  id,
  className,
  lang,
  items,
  itemsToDisplay,
  error,
  errorMessage,
  gaClickEvent,
  gaActionText,
}) => {
  /**
  * isDataInvalid(data errorFlag)
  * returns boolean check if the data array exists or if the error flag is true.
  */
  const isDataInvalid = (data, errorFlag) => (
    (!data || !data.length || errorFlag)
  );

  /**
   * renderContentTitle(bemName, title, language) {
   * returns the DOM representation of the main content title.
   * Each property is namespaced via bemName and verified for existance.
   */
  const renderContentTitle = (bemName, title, language) => (
    (title && title[language] && title[language].text) ?
      <div className={`${bemName}-title`}>{title[language].text}</div> : null
  );

  /**
   * renderContentImage(bemName, image)
   * returns the DOM representation of an image only if the data exists,
   * namespacing the DOM via bemName for convention.
   */
  const renderContentImage = (bemName, image) => {
    const imageUri = (image && image.rectangularImage && image.rectangularImage['full-uri']) ?
      image.rectangularImage['full-uri'] : null;
    const alt = (image && image.rectangularImage && image.rectangularImage.alt) ?
      image.rectangularImage.alt : '';

    return (imageUri) ?
      <img src={imageUri} alt={alt} className={`${bemName}-image rectangularImage`} /> : null;
  };

  /**
   * renderContentDesc(bemName, description, language) {
   * returns the DOM representation of the main content description
   * composed of the description and language properties.
   * Each property is namespaced via bemName and verified for existance.
   */
  const renderContentDesc = (bemName, description, language) => (
    (description && description[language] && description[language].text) ?
      <p className={`${bemName}-description`}>{description[language].text}</p> : null
  );

  /**
   * generateItemsToDisplay(data)
   * returns the DOM for each item to be displayed by iterating
   * through the data. Elements to be displayed are constructed
   * from helper render methods. Namespacing the component via
   * the BEM name and sets a limit of iteration through itemsToDisplay.
   */
  const generateItemsToDisplay = (data) => (
    data.slice(0, itemsToDisplay).map((element, i) => {
      const {
          title,
          link: url = '#',
          image,
          description: desc,
      } = element;
      const mainImage = renderContentImage(className, image);
      const contentTitle = renderContentTitle(className, title, lang);
      const contentDesc = renderContentDesc(className, desc, lang);
      const gaAction = gaActionText ? `${gaActionText} - ${i + 1}` : '';

      return (
        <li className={`${className}-wrapper`} key={i}>
          <a
            className={`${className}-link`}
            href={url}
            onClick={gaClickEvent && gaActionText ? () => gaClickEvent(gaAction, url) : null}
          >
            {mainImage}
            {contentTitle}
            {contentDesc}
          </a>
        </li>
      );
    })
  );

  /**
   * renderFailure(msg)
   * returns a failure DOM container with an error
   * message passed as a parameter
   */
  const renderFailure = (msg) => (
    <div className="errorMessage">
      {msg}
    </div>
  );

  /**
   * renderSuccess(data)
   * returns a success DOM container where the
   * component data is passed to _generateItemsToDisplay()
   * for proper iteration and rendering.
   */
  const renderSuccess = (data) => (
    <ul className={className} id={id}>
      {generateItemsToDisplay(data)}
    </ul>
  );

  return isDataInvalid(items, error) ? renderFailure(errorMessage) : renderSuccess(items);
};

FeatureRow.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  lang: PropTypes.string,
  items: PropTypes.array,
  itemsToDisplay: PropTypes.number,
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  gaClickEvent: PropTypes.func,
  gaActionText: PropTypes.string,
};

FeatureRow.defaultProps = {
  className: 'FeatureRow',
  errorMessage: 'We\'re sorry. Information isn\'t available for this feature.',
  lang: 'en',
  itemsToDisplay: 2,
};

export default FeatureRow;
