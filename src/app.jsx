import React from 'react';
import { render } from 'react-dom';
import FeatureRow from './component.jsx';

const dummyContent = [
  {
    title: {
      en: {
        text: 'title01',
      },
      es: {},
    },
    description: {
      en: {
        text: 'description01',
      },
      es: {},
    },
    image: {
      bannerImage: null,
      rectangularImage: {
        'full-uri': 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
        description: 'This is the image description',
        alt: 'This is the image description',
      },
    },
    link: 'http://www.nypl.org',
  }, {
    title: {
      en: {
        text: 'title02',
      },
      es: {},
    },
    description: {
      en: {
        text: 'description02',
      },
      es: {},
    },
    image: {
      bannerImage: null,
      rectangularImage: {
        'full-uri': 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
        description: 'This is the image description',
        alt: 'This is the image description',
      },
    },
    link: 'http://www.nypl.org',
  },
  {
    title: {
      en: {
        text: 'title03',
      },
      es: {},
    },
    description: {
      en: {
        text: 'description03',
      },
      es: {},
    },
    image: {
      bannerImage: null,
      rectangularImage: {
        'full-uri': 'http://cdn-prod.www.aws.nypl.org/sites/default/files/FW.jpg',
        description: 'This is the image description',
        alt: 'This is the image description',
      },
    },
    link: 'http://www.nypl.org',
  },
];

// Used to mock gaClick event
const gaClickTest = () => (
  (action, label) => {
    console.log(action);
    console.log(label);
  }
);

/* app.jsx
 * Used for local development of React Components
 */
render(
  <FeatureRow
    id="hpOfNote"
    className="hpOfNote"
    items={dummyContent}
    gaClickEvent={gaClickTest()}
    gaActionText="Of Note"
  />,
  document.getElementById('component')
);
