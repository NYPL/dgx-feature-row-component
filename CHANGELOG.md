## Changelog

### v0.2.0
#### Updated
- React version to v15.

### v0.1.2
#### Added
- Added support for Google Analytics click events via `gaClickEvent` function property.
- Added README.md file

#### Changed
- Refactored component to state-less composition.
